<?php

/**
 * @file
 *   Contains model functions, that are functions used to directly communicate with the database
 *  using db_query() and related functions.
 */

/**
 * Saves an invitation that is sent to an user on a group.
 *
 * @param object $invitation
 *   An object that maps over the {og_invite} table
 */
function og_invite_link_save_invitation($invitation) {
  if ($invitation->is_new) {
    db_query("INSERT INTO {og_invite} (uid, group_nid, invite_key, timestamp, accepted_timestamp) VALUES(%d, %d, '%s', %d, 0)",
    $invitation->uid, $invitation->nid, $invitation->invite_key, isset($invitation->timestamp)?$invitation->timestamp:$_SERVER['REQUEST_TIME']);
  }
  else {
    db_query("UPDATE {og_invite} SET accepted_timestamp = %d WHERE uid=%d AND group_nid=%d",
    isset($invitation->accepted_timestamp)?$invitation->accepted_timestamp:0, $invitation->uid, $invitation->nid);
  }
}

/**
 * Checks if an user is already invited into a group.
 *
 * @param int $user_id
 *   The user id to be checked.
 *
 * @param int $group_id
 *   The group id to be checked.
 */
function og_invite_link_user_is_invited($user_id, $group_id) {
  return db_fetch_object(db_query('SELECT uid, group_nid, invite_key, timestamp, accepted_timestamp FROM {og_invite} WHERE uid=%d AND group_nid=%d', $user_id, $group_id));
}

/**
 * Returns all the invitations of an user.
 *
 * @param int $user_id
 *   The user id.
 */
function og_invite_link_get_all_invitations($user_id, $reset = FALSE) {
  static $invitations;
  if (!isset($invitations[$user_id]) || $reset) {
    $result = db_query("SELECT group_nid FROM {og_invite} WHERE uid=%d ORDER BY timestamp DESC", $user_id);
    $invitations[$user_id] = array();
    while ($data = db_fetch_object($result)) {
      $invitations[$user_id][$data->group_nid] = $data;
    }
  }
  return $invitations[$user_id];
}

/**
 * Returns an array with the invited members in a group
 *
 * @param int $node_id
 *   The id of the group
 *
 * @param array $conds
 *   An array with conditions (to be defined).
 *
 * @param array $pager
 *   An array with pager details (limit, element)
 */
function og_invite_link_get_group_invited_members($node_id, $conds = array(), $pager = array()) {
  $inners[] = " INNER JOIN {users} u ON u.uid=oiu.uid ";
  $fields[] = " oiu.uid AS user_id ";
  $fields[] = " oiu.timestamp, oiu.accepted_timestamp";
  $wheres[] = " u.status = 1";
  $wheres[] = " oiu.group_nid=%d ";
  $args[] = $node_id;
  if ($conds['username']) {
    $wheres[] = ' u.name LIKE "%%%s%%" ';
    $args[] = $conds['username'];
  }
  $sql = "SELECT " . implode(',', $fields) . ' FROM {og_invite} oiu ' . implode(' ', $inners) . ' WHERE ' . implode(' AND ', $wheres) . " ORDER BY u.name ASC, oiu.timestamp DESC";
  $result = NULL;
  if ($pager['paged']) {
    $count_query = 'SELECT COUNT(oiu.uid)  FROM {og_invite} oiu ' . implode(' ', $inners) . ' WHERE ' . implode(' AND ', $wheres);
    $result = pager_query($sql, isset($pager['limit'])?$pager['limit']:10, isset($pager['element'])?$pager['element']:0, $count_query, $args);
  }
  else {
    $result = db_query($sql, $args);
  }
  $members = array();
  while ($data = db_fetch_object($result)) {
    $members[$data->user_id] = $data;
  }
  return $members;
}

/**
 * Returns the number of total sent invitations
 *
 * @param int $node_id
 *   The group id to check for.
 */
function og_invite_link_get_total_invitations($node_id) {
  return db_result(db_query("SELECT COUNT(oiu.uid) AS total FROM {og_invite} oiu INNER JOIN {users} u ON u.uid = oiu.uid
                              WHERE oiu.group_nid=%d AND u.status=1", $node_id));
}

function og_invite_link_get_total_invitations_accepted($node_id) {
  return db_result(db_query("SELECT COUNT(oiu.uid) AS total FROM {og_invite} oiu INNER JOIN {users} u ON u.uid = oiu.uid
                              WHERE oiu.group_nid=%d AND u.status=1 AND oiu.accepted_timestamp > 0", $node_id));
}