<?php

/**
 * @file
 * 	Contains the callback functions for this module (like invite page form, or join page callback).
 */

/**
 * Member invite page for an organic group. The form on this invite page will send join links to the invited users.
 *
 * @param object $node
 *   The organic group node.
 */
function og_invite_link_invite_page($node) {
  return drupal_get_form('og_invite_link_invite_page_form', $node);
}

/**
 * Form constructor for inviting members into groups.
 *
 */
function og_invite_link_invite_page_form($form_state = array(), $node) {
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['invitees'] = array(
    '#type' => 'textarea',
    '#title' => t('Invitees'),
    '#required' => TRUE,
    '#description' => t('Add one or more usernames in order to invite users in this group. Multiple usernames should be separated by a comma.'),
  );
  $form['additional_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional message'),
    '#description' => t('Insert an additional message to be sent to the users.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Invite'),
  );
  return $form;
}

function og_invite_link_invite_page_form_submit($form, &$form_state) {
  //Get the ids of the users based on the name that was inserted.
  //TO DO: think if it would be good to check for already invited members?
  //This check is currently made in og_invite_link_send_invites function, in og_invite_link.inc file, and messages are set in those cases.
  $names = explode(',', $form_state['values']['invitees']);
  foreach ($names as $key => $name) {
    $names[$key] = trim($name);
  }
  $result = db_query("SELECT u.uid FROM {users} u WHERE u.status=1 AND u.name IN('" . implode("','", $names) . "')");
  $uids = array();
  while ($data = db_fetch_object($result)) {
    $uids[] = $data->uid;
  }
  if (count($form_state['values']['results'])) {
    $uids = array_merge($uids, $form_state['values']['results']);
  }
  $group = node_load($form_state['values']['nid']);
  og_invite_link_inc('og_invite_link_send_invites', $group, $uids, $form_state['values']['additional_message']);
}

/**
 * Page callback for joining a group
 *
 * @param object $node
 *   The group node object.
 *
 * @param object $account
 *   The user object.
 *
 * @param string $hash
 *   The hash that has to be validated.
 */
function og_invite_link_join($node, $account, $hash) {
  global $user;
  //First, login the user if he is not yet logged in.
  //Is it the right way to do that?
  if (!$user->uid) {
    $user = user_load(array('uid' => $account->uid));
  }
  //To DO: if I am logged in as an user, but access a valid link for another user, what should happen? Should I be logged in with the user that received the invitation?
  // Use og_save_subscription() to get around og_subscribe_user()'s
  // selectivity checks.
  //TO DO: is this a good thing?
  og_save_subscription($node->nid, $account->uid, array('is_active' => 1));
  $invitation = new stdClass();
  $invitation->uid = $account->uid;
  $invitation->nid = $node->nid;
  $invitation->accepted_timestamp = $_SERVER['REQUEST_TIME'];
  og_invite_link_model('og_invite_link_save_invitation', $invitation);
  drupal_set_message(t('You are a member of this group now.'));
  drupal_goto('node/' . $node->nid);
}