<?php

/**
 * @file
 * 	Contains utility functions for this module.
 */

/**
 * Sends invitation mails to a set of users for a group.
 *
 * @param object $group
 *   The group node object.
 *
 * @param array $uids
 *   An array with user ids to be invited.
 *
 * @param string $additional_message
 *   The additional message to be put in the mail.
 */
function og_invite_link_send_invites($group, $uids, $additional_message) {
  $index = 0;
  $not_invited = 0;
  foreach ($uids as $uid) {
    $account = user_load(array('uid' => $uid));
    if (is_object($account)) {
      //Check if the user is already invited, and if yes do not invite him again.
      $invited = og_invite_link_model('og_invite_link_user_is_invited', $uid, $group->nid);
      if (is_object($invited)) {
        $not_invited++;
        drupal_set_message(t("The user !username has been already invited into the group @group. He was not invited again.",
            array('!username' => theme('username', $account), '@group' => $group->title)), 'error');
        continue;
      }
      $index++;
      //Store that the user has been invited.
      $invitation = new stdClass();
      $invitation->is_new = TRUE;
      $invitation->nid = $group->nid;
      $invitation->uid = $account->uid;
      //To generate a random key, just use the user_password function.
      $invitation->invite_key = user_password();
      $invitation->timestamp = $_SERVER['REQUEST_TIME'];
      og_invite_link_model('og_invite_link_save_invitation', $invitation);
      //Send the mail.
      drupal_mail('og_invite_link', 'invite_to_group', $account->mail, language_default(), array('group' => $group, 'account' => $account,
                                                                                  'additional_message' => $additional_message, 'invitation' => $invitation));
      //Also, store an entry in watchdog.
      watchdog('og_invite_link', "Mail sent to !mail (user id: !user_id) for inviting in group !group",
                array('!mail' => $account->mail, '!user_id' => $account->uid, '!group' => $group->title), WATCHDOG_INFO );
    }
    else {
      watchdog('og_invite_link', "Trying to invite an invalid user with id: !user_id in group !group",
                array('!user_id' => $uid, '!group' => $group->title), WATCHDOG_ERROR);
    }
  }
  drupal_set_message(t('!number users have been invited to the group', array('!number' => $index)));
  if ($not_invited) {
    drupal_set_message(t('!number users have NOT been invited to the group', array('!number' => $not_invited)));
  }
}